# Порядок установки компонентов программы - Платформа Windows

## Установка Erlang
Скачать последнюю версию можно по ссылке: https://www.erlang.org/downloads
После скачивания установить программу без изменения параметров

## Установить RabbitMQ
Скачать последнюю версию можно оп ссылке: https://www.rabbitmq.com/install-windows.html
Ссылку на скачивание .exe установки можно найти в подпункте "Direct Downloads"
Установка rabbitmq-server-{version}.exe с параметрами по умолчанию.
RabbitMQ уже запущен.

## Настройка RabbitMQ
- Настройка web-интерфейса RabbitMQ
    Найти RabbitMq Command Prompt через поиск windows и запустить от имени администратора
    В открывшейся консоли нужно выполнить команду rabbitmq-plugins.bat enable rabbitmq_management
    После окончания выполнения перезапустить сервис, поочерёдно выполняя команды:
      rabbitmq-service.bat stop
      rabbitmq-service.bat install
      rabbitmq-service.bat start
    Проверка работы web-интерфейса. Перейти по адресу http://localhost:15672 (значение по умолчанию)
    Логин: guest
    пароль: guest
  
Более подробно об установке можно прочитать по ссылке: https://badcode.ru/ustanovka-rabbitmq-na-windows-10/
  
- Настройка виртуального хоста и очереди
    Открыть вкладку Admin
	Нажать Add a new virtual host
	В поле Name написать ABMessage и нажать Add virtual host
	
	Открыть вкладку Queues
	Необходимо добавить 2 очереди для текущей реализации программы
	Add a new queue
	   Virtual host -> ABMessage(Для обеих)
	   Name -> ABMessage(для первой), ACMessage(для второй)
	   Остальные параметры оставить по умолчанию
	   Add queue
	   
- На вкладке Overview можно увидеть порты RabbitMQ
     У протокола ampq развернутого связанного с 0.0.0.0 должен быть порт 5672.
  
## Запуск программы
	В директории ..\rabbittestproject\FirstWeb
	Необходимо создать директорию Storage
	Создать 2 файла-хранилища 
		..\rabbittestproject\FirstWeb\MessageStorage2.txt
		..\rabbittestproject\FirstWeb\MessageStorage3.txt

	Открыть 3 версии FirstWeb.sln
	Назначить автозапускаемым проектом PublisherA(для первого), SubscriberB(для второго), SubscriberC(для третьего)
	Запустить все проекты, порядок не важен, для каждого проекта откроется swagger.
	
	Для отправки сообщения используется swagger PublisherA
	- SendToAll - отправить сообщения обоим сервисам
	- SendToB - отправить в сервис SubscriberB
	- SendToC - отправить в сервис SubscriberC
	
	Для отображения сообщений используется SubscriberB и SubscriberC
	- last  - отобразить последнее сохранённое сообщение
	- all  - отобразить все сохранённые сообщение
	
  