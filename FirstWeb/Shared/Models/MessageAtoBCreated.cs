﻿using EventBus.Base.Standard;

namespace Shared.Models
{
    /// <summary>
    /// Сообщение из A в B. Кладётся в очередь для SubscriberB
    /// </summary>
    public class MessageAtoBCreated : IntegrationEvent
    {
        /// <summary>
        /// Просто номер
        /// </summary>
        public int TotalMessageNumber { get; set; }
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        public string GetMessage()
        {
            return $"Сообщение номер {TotalMessageNumber}: {Message}";
        }
    }
}
