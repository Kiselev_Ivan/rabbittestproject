﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared.Storages.FileStorage
{
    /// <summary>
    /// Интерфейс работы с хранилищем
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// Добавить сообщение
        /// </summary>
        /// <param name="message"> Сообщение </param>
        public Task AddMessage(string message);

        /// <summary>
        /// Прочитать все добавленные сообщения
        /// </summary>
        public Task<List<string>> ReadAllMessages();
    }
}
