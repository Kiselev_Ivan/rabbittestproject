using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Shared.Storages.FileStorage
{
    /// <summary>
    /// ��������� ������ ���� ����
    /// </summary>
    public class FileStorage : IStorage
    {
        protected readonly FilePath filePath;

        public FileStorage(IOptions<FilePath> path)
        {
            filePath = path.Value;
        }

        public async Task AddMessage(string message)
        {
            if (!File.Exists(filePath.Path))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath.Path));
                File.Create(filePath.Path);
            }

            using (var file = new StreamWriter(filePath.Path, true))
            {
                await file.WriteLineAsync(message);
            }
        }

        public async Task<List<string>> ReadAllMessages()
        {
            List<string> result = new();

            try
            {
                using (var file = new StreamReader(filePath.Path))
                {
                    while (!file.EndOfStream)
                    {
                        var message = await file.ReadLineAsync();
                        if (!string.IsNullOrEmpty(message))
                            result.Add(message);
                    }
                }
            }
            catch
            {
                return new List<string>();
            }

            return result;
        }
    }
}
