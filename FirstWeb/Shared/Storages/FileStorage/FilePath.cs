﻿namespace Shared.Storages.FileStorage
{
    /// <summary>
    /// Путь к хранилищу (получается их конфигурации)
    /// </summary>
    public class FilePath
    {
        public string Path { get; set; }
    }
}
