﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared.Services
{
    /// <summary>
    /// Интерфейс сервиса работы с данными
    /// </summary>
    public interface IDataService
    {
        /// <summary>
        /// Добавить сообщение
        /// </summary>
        /// <param name="message"> Сообщение </param>
        public Task AddMessage(string message);

        /// <summary>
        /// Вернуть все сообщения
        /// </summary>
        /// <returns> Json всех сообщений </returns>
        public Task<List<string>> GetAllMessages();
    }
}
