﻿using Shared.Storages.FileStorage;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace Shared.Services
{
    /// <summary>
    /// Сервис работы с данными
    /// </summary>
    public class DataService : IDataService
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        protected IStorage _storage;

        public DataService(IStorage storage)
        {
            _storage = storage;
        }

        public async Task AddMessage(string message)
        {
            await _storage.AddMessage(message);
        }

        public async Task<List<string>> GetAllMessages()
        {
            return await _storage.ReadAllMessages();
        }
    }
}
