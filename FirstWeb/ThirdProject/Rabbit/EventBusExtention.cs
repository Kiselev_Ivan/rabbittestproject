﻿using EventBus.Base.Standard;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Shared.Models;
using System.Collections.Generic;

namespace SubscriberC.Rabbit
{
    public static class EventBusExtension
    {
        /// <summary>
        /// Создание обработчика событий
        /// </summary>
        public static IEnumerable<IIntegrationEventHandler> GetHandlers()
        {
            return new List<IIntegrationEventHandler>
            {
                new ItemCreatedIntegrationEventHandler()
            };
        }

        /// <summary>
        ///  Подписка на событие
        /// </summary>
        public static IApplicationBuilder SubscribeToEvents(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<MessageAtoCCreated, ItemCreatedIntegrationEventHandler>();

            return app;
        }
    }
}
