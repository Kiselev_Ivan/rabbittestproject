﻿using EventBus.Base.Standard;
using Shared.Models;
using System;
using System.Threading.Tasks;

namespace SubscriberC.Rabbit
{
    /// <summary>
    /// Обработчик событий
    /// </summary>
    public class ItemCreatedIntegrationEventHandler : IIntegrationEventHandler<MessageAtoCCreated>
    {
        public delegate void MessageResive(string message);

        /// <summary>
        /// event по умолчанию добавляет к делегату вывод в консоль
        /// (в случае если никто больше не подпишется на событие событие всё равно обработается)
        /// </summary>
        public static event MessageResive Recive = ((string message)=>Console.WriteLine(message));

        public ItemCreatedIntegrationEventHandler()
        {
        }

        /// <summary>
        /// Обработка возникновения события
        /// </summary>
        public Task Handle(MessageAtoCCreated @event)
        {
            Recive(@event.GetMessage());
            return Task.CompletedTask;
        }
    }
}
