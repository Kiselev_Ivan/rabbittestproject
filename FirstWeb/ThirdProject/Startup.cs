using EventBus.Base.Standard.Configuration;
using EventBus.RabbitMQ.Standard.Configuration;
using EventBus.RabbitMQ.Standard.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Shared.Services;
using Shared.Storages.FileStorage;
using SubscriberC.Rabbit;

namespace SubscriberC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SubscriberC", Version = "v1" });
            });

            CreateDI(services);
            AddRabbitMQ(services);
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // Запуск swagger в при launchSetings.profiles.IIS Express
                //app.UseSwagger();
                //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SubscriberC v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.SubscribeToEvents();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default",
                    "{controller=WriteMessage}/{action=Index}/{id?}");
            });
        }

        private void CreateDI(IServiceCollection services)
        {
            services.AddScoped<IDataService, DataService>();
            services.AddScoped<IStorage, FileStorage>();

            services.Configure<FilePath>(Configuration.GetSection("FileStoragePath"));
        }

        private void AddRabbitMQ(IServiceCollection services)
        {
            var rabbitMqOptions = Configuration.GetSection("RabbitMq").Get<RabbitMqOptions>();

            services.AddRabbitMqConnection(rabbitMqOptions);
            services.AddRabbitMqRegistration(rabbitMqOptions);
            services.AddEventBusHandling(EventBusExtension.GetHandlers());

            // Вероятно данный подход не профессионален, но из того, что у меня вышло он наиболее понятный
            var provider = services.BuildServiceProvider();
            var dataService = provider.GetRequiredService<IDataService>();
            _ = new RabbitHandler(dataService);
        }
    }
}
