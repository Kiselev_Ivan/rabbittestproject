﻿using Microsoft.AspNetCore.Mvc;
using Shared.Services;
using System.Threading.Tasks;

namespace SubscriberC.Controllers
{
    public class WriteMessageController : Controller
    {
        private readonly IDataService _dataService;

        public WriteMessageController(IDataService dataService)
        {
            _dataService = dataService;
        }

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Получить все записанные сообщения
        /// </summary>
        [HttpGet]
        public async Task<string> GetAll()
        {
            return string.Join("<br/>", await _dataService.GetAllMessages());
        }
    }
}
