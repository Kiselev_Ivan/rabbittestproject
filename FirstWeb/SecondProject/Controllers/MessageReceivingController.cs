﻿using Microsoft.AspNetCore.Mvc;
using Shared.Services;
using Shared.Storages.FileStorage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SubscriberC.Controllers
{
    public class MessageReceivingController : Controller
    {
        private readonly IDataService _dataService;

        public MessageReceivingController(IDataService dataService)
        {
            _dataService = dataService;
        }

        public IActionResult Index()
        {            
            return View();
        }

        /// <summary>
        /// Получить все записанные сообщения
        /// </summary>
        [HttpGet]
        public async Task<string> GetAll()
        {

            return string.Join("<br/>",await _dataService.GetAllMessages());
        }
    }
}
