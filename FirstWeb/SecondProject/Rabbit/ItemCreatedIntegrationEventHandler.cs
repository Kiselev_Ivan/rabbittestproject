﻿using EventBus.Base.Standard;
using Shared.Models;
using System;
using System.Threading.Tasks;

namespace SubscriberB.Rabbit
{
    /// <summary>
    /// Обработчик событий
    /// </summary>
    public class ItemCreatedIntegrationEventHandler : IIntegrationEventHandler<MessageAtoBCreated>
    {
        public delegate void MessageRecive(string message);

        /// <summary>
        /// event по умолчанию добавляет к делегату вывод в консоль
        /// (в случае если никто больше не подпишется на событие событие всё равно обработается)
        /// </summary>
        public static event MessageRecive Recive = ((string message) => Console.WriteLine(message));
        
        public ItemCreatedIntegrationEventHandler()
        {
        }

        /// <summary>
        /// Обработка возникновения события
        /// </summary>
        public Task Handle(MessageAtoBCreated @event)
        {
            Recive(@event.GetMessage());
            return Task.CompletedTask;
        }
    }
}
