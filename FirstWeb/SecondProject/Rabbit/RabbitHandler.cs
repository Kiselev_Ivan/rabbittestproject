﻿using Shared.Services;

namespace SubscriberB.Rabbit
{
    /// <summary>
    /// Класс-обработчик для возникновения события
    /// </summary>
    public class RabbitHandler
    {
        private readonly IDataService _dataService;

        public RabbitHandler(IDataService dataService)
        {
            _dataService = dataService;
            ItemCreatedIntegrationEventHandler.Recive += AddMessage;
        }

        public void AddMessage(string message)
        {
            _dataService.AddMessage(message);
        }
    }
}
