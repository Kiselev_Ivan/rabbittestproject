﻿namespace PublisherA.Models.Enums
{
    /// <summary>
    /// Получатели сообщений
    /// </summary>
    public enum MessageRecivers
    {
        /// <summary>
        /// Все
        /// </summary>
        All = 0,
        /// <summary>
        /// Получатель подписчик B
        /// </summary>
        B = 1,
        /// <summary>
        /// Получатель подписчик C
        /// </summary>
        C = 2
    }
}
