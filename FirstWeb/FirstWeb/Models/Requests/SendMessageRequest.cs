﻿using PublisherA.Models.Enums;

namespace PublisherA.Models.Requests
{
    /// <summary>
    /// Запрос на отправку сообщения
    /// </summary>
    public class SendMessageRequest
    {
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Тип получателя
        /// </summary>
        public MessageRecivers Reciver { get; set; }
    }
}
