﻿using Microsoft.AspNetCore.Mvc;
using PublisherA.Service.Rabbit;
using PublisherA.Models.Requests;

namespace PublisherA.Controllers
{
    public class MessagesController : Controller
    {
        private readonly IRabbitService _rabbitService;

        public MessagesController(IRabbitService rabbitService)
        {
            _rabbitService = rabbitService;
        }

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="sendRequest"> SendMessageRequest содержит в себе текст сообщения и получателя(-ей) сообщения </param>
        [HttpPost]
        public void Send(SendMessageRequest sendRequest)
        {
            if (sendRequest != null)
                _rabbitService.SendMessage(sendRequest);
        }
    }
}
