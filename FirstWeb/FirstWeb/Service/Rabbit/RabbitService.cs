﻿using EventBus.Base.Standard;
using PublisherA.Models.Enums;
using PublisherA.Models.Requests;
using Shared.Models;

namespace PublisherA.Service.Rabbit
{
    public class RabbitService : IRabbitService
    {
        private readonly IEventBus _eventBus;
        private int _messageNumber = 0;

        public RabbitService(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public void SendMessage(SendMessageRequest message)
        {
            _messageNumber++;
            switch (message.Reciver)
            {
                case MessageRecivers.All:
                    SendAll(message.Message);
                    break;
                case MessageRecivers.B:
                    SendToB(message.Message);
                    break;
                case MessageRecivers.C:
                    SendToC(message.Message);
                    break;
                default: throw new System.Exception("Непредвиденный тип");
            }
        }

        private void SendAll(string message)
        {
            _eventBus.Publish(CreateMessageAtoB(message));
            _eventBus.Publish(CreateMessageAtoC(message));
        }

        private void SendToB(string message)
        {

            _eventBus.Publish(CreateMessageAtoB(message));
        }

        private void SendToC(string message)
        {
            _eventBus.Publish(CreateMessageAtoC(message));
        }

        private MessageAtoBCreated CreateMessageAtoB(string message)
        {
            return new MessageAtoBCreated()
            {
                TotalMessageNumber = _messageNumber,
                Message = message
            };
        }

        private MessageAtoCCreated CreateMessageAtoC(string message)
        {
            return new MessageAtoCCreated()
            {
                TotalMessageNumber = _messageNumber,
                Message = message
            };
        }
    }
}
