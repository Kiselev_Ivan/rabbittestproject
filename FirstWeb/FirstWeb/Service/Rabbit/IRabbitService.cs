﻿using PublisherA.Models.Enums;
using PublisherA.Models.Requests;

namespace PublisherA.Service.Rabbit
{
    public interface IRabbitService
    {
        /// <summary>
        /// Отправить сообщение получател(ю)ям
        /// </summary>
        /// <param name="message"> Текст сообщения </param>
        /// <param name="messageReciver"> Получатель сообщения </param>
        public void SendMessage(SendMessageRequest message);

    }
}
